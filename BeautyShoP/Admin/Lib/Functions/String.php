<?php
	function quotedString($str){
		if(ini_get("magic_quotes_gpc")){
			return  $str;
		}else{
			return addslashes($str);
		}
	}
	
	function unQuoteString($str){
		if(ini_get("magic_quotes_gpc")){
			return  $str;
		}else{
			return stripslashes($str);
		}
	}
	
	function generatePassword($cnt){
		$str = "ABCEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz0123456789";
$shu_str =  str_shuffle($str);
		return substr($shu_str,0,$cnt);
	}
?>