-- phpMyAdmin SQL Dump
-- version 4.8.5
-- https://www.phpmyadmin.net/
--
-- Host: 127.0.0.1
-- Generation Time: Feb 25, 2020 at 06:09 AM
-- Server version: 10.1.38-MariaDB
-- PHP Version: 5.6.40

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET AUTOCOMMIT = 0;
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `beautyshop`
--

-- --------------------------------------------------------

--
-- Table structure for table `bs_admin`
--

CREATE TABLE `bs_admin` (
  `Id` int(5) UNSIGNED NOT NULL,
  `AdminName` varchar(20) NOT NULL,
  `Password` char(32) NOT NULL
) ENGINE=MyISAM DEFAULT CHARSET=latin1;

--
-- Dumping data for table `bs_admin`
--

INSERT INTO `bs_admin` (`Id`, `AdminName`, `Password`) VALUES
(1, 'admin', '21232f297a57a5a743894a0e4a801fc3'),
(2, 'hritik', '7012ef0335aa2adbab58bd6d0702ba41');

-- --------------------------------------------------------

--
-- Table structure for table `bs_categories`
--

CREATE TABLE `bs_categories` (
  `Id` int(5) UNSIGNED NOT NULL,
  `Name` varchar(20) NOT NULL,
  `cStatus` tinyint(1) NOT NULL DEFAULT '0'
) ENGINE=MyISAM DEFAULT CHARSET=latin1;

--
-- Dumping data for table `bs_categories`
--

INSERT INTO `bs_categories` (`Id`, `Name`, `cStatus`) VALUES
(11, 'computer science', 0);

-- --------------------------------------------------------

--
-- Table structure for table `bs_customers`
--

CREATE TABLE `bs_customers` (
  `Id` int(5) UNSIGNED NOT NULL,
  `Name` varchar(20) NOT NULL,
  `EmailId` varchar(50) NOT NULL,
  `Password` char(32) NOT NULL,
  `RegisteredOn` varchar(20) NOT NULL,
  `cStatus` tinyint(1) NOT NULL DEFAULT '0'
) ENGINE=MyISAM DEFAULT CHARSET=latin1;

--
-- Dumping data for table `bs_customers`
--

INSERT INTO `bs_customers` (`Id`, `Name`, `EmailId`, `Password`, `RegisteredOn`, `cStatus`) VALUES
(1, 'Sailesh', 'jaiswalsailesh@gmail.com', 'e10adc3949ba59abbe56e057f20f883e', '1223121852', 1),
(3, 'Sailesh', 'findinurheart@yahoo.co.in', 'e10adc3949ba59abbe56e057f20f883e', '1223377921', 1),
(4, 'sripriya222', 'sripriya222@gmail.com', 'dffb394d51120b7ee347736582367f3a', '1226749000', 0),
(5, 'Jaiswal', 'lampsailesh@gmail.com', 'e10adc3949ba59abbe56e057f20f883e', '1227180189', 1),
(6, 'subbu', 'subbu_aakula@yahoo.com', '0e84c9dee6696774b4bc053f1b9b26df', '1227501530', 0),
(7, 'sripriya', 'sripriya222@gmail.com', '3480353ed294e9e5a4cd1d7165375071', '1227533483', 0),
(8, 'kumar', 'satisu@gmail.com', '0c15baf25172b2e3c53513af3f172575', '1229425911', 0),
(9, 'kumar', 'satisu@gmail.com', '0c15baf25172b2e3c53513af3f172575', '1229425950', 0),
(10, 'Ramesh', 'ramesshnaidu1985@gmail.com', 'c246ad314ab52745b71bb00f4608c82a', '1230234634', 0),
(11, 'Sanjeev', 'veejnas@yahoo.com', 'e10adc3949ba59abbe56e057f20f883e', '1237802538', 1);

-- --------------------------------------------------------

--
-- Table structure for table `bs_orders`
--

CREATE TABLE `bs_orders` (
  `Id` int(5) UNSIGNED NOT NULL,
  `oDate` varchar(20) NOT NULL,
  `Amount` decimal(12,2) NOT NULL,
  `oStatus` tinyint(1) NOT NULL DEFAULT '0',
  `CustId` int(5) UNSIGNED NOT NULL
) ENGINE=MyISAM DEFAULT CHARSET=latin1;

--
-- Dumping data for table `bs_orders`
--

INSERT INTO `bs_orders` (`Id`, `oDate`, `Amount`, `oStatus`, `CustId`) VALUES
(1, '1223290792', '2700.00', 2, 1),
(2, '1223292550', '3150.00', 1, 1),
(3, '1223377466', '1000.00', 1, 1),
(4, '1229482765', '1200.00', 2, 3),
(5, '1230901621', '600.00', 1, 1),
(6, '1230901768', '200.00', 0, 1),
(7, '1232186539', '1000.00', 1, 3),
(8, '1232596020', '1200.00', 1, 3),
(9, '1232607065', '1000.00', 1, 3),
(10, '1237802403', '200.00', 0, 3),
(11, '1237802992', '2500.00', 2, 3);

-- --------------------------------------------------------

--
-- Table structure for table `bs_order_products`
--

CREATE TABLE `bs_order_products` (
  `OrderId` int(5) UNSIGNED NOT NULL,
  `ProductId` int(5) UNSIGNED NOT NULL,
  `Quantity` int(3) UNSIGNED NOT NULL
) ENGINE=MyISAM DEFAULT CHARSET=latin1;

--
-- Dumping data for table `bs_order_products`
--

INSERT INTO `bs_order_products` (`OrderId`, `ProductId`, `Quantity`) VALUES
(1, 6, 2),
(1, 4, 1),
(1, 3, 2),
(2, 1, 1),
(2, 5, 1),
(2, 7, 1),
(2, 9, 1),
(0, 6, 1),
(3, 6, 1),
(4, 1, 1),
(4, 4, 2),
(5, 4, 1),
(5, 3, 1),
(6, 1, 1),
(7, 6, 1),
(8, 1, 1),
(8, 6, 1),
(9, 6, 1),
(10, 1, 1),
(11, 6, 2),
(11, 4, 1);

-- --------------------------------------------------------

--
-- Table structure for table `bs_products`
--

CREATE TABLE `bs_products` (
  `Id` int(5) UNSIGNED NOT NULL,
  `Name` varchar(20) NOT NULL,
  `Price` decimal(10,2) NOT NULL,
  `ImagePath` varchar(100) NOT NULL,
  `Description` text NOT NULL,
  `CatId` int(5) UNSIGNED NOT NULL,
  `pStatus` tinyint(1) NOT NULL DEFAULT '0'
) ENGINE=MyISAM DEFAULT CHARSET=latin1;

--
-- Dumping data for table `bs_products`
--

INSERT INTO `bs_products` (`Id`, `Name`, `Price`, `ImagePath`, `Description`, `CatId`, `pStatus`) VALUES
(1, 'Braslet', '200.00', 'Products/1222928266_braslet.jpg', 'PHP succeeds an older product, named PHP/FI. PHP/FI was created by Rasmus Lerdorf in 1995, initially as a simple set of Perl scripts for tracking accesses to his online resume. He named this set of scripts \'Personal Home Page Tools\'. As more functionality was required, Rasmus wrote a much larger C implementation, which was able to communicate with databases, and enabled users to develop simple dynamic Web applications. Rasmus chose to » release the source code for PHP/FI for everybody to see, so that anybody can use it, as well as fix bugs in it and improve the code. \r\n\r\n', 5, 1),
(2, 'Ear-rings', '100.00', 'Products/1222928309_hererings.jpg', 'PHP/FI, which stood for Personal Home Page / Forms Interpreter, included some of the basic functionality of PHP as we know it today. It had Perl-like variables, automatic interpretation of form variables and HTML embedded syntax. The syntax itself was similar to that of Perl, albeit much more limited, simple, and somewhat inconsistent.', 5, 1),
(3, 'Tops', '100.00', 'Products/1222928343_top2.jpg', 'By 1997, PHP/FI 2.0, the second write-up of the C implementation, had a cult of several thousand users around the world (estimated), with approximately 50,000 domains reporting as having it installed, accounting for about 1% of the domains on the Internet. While there were several people contributing bits of code to this project, it was still at large a one-man project. \r\n\r\n', 5, 1),
(4, 'LongRed', '500.00', 'Products/1222928431_t-shirt2.jpg', 'PHP 3.0 was the first version that closely resembles PHP as we know it today. It was created by Andi Gutmans and Zeev Suraski in 1997 as a complete rewrite, after they found PHP/FI 2.0 severely underpowered for developing an eCommerce application they were working on for a University project. In an effort to cooperate and start building upon PHP/FI\'s existing user-base, Andi, Rasmus and Zeev decided to cooperate and announce PHP 3.0 as the official successor of PHP/FI 2.0, and development of PHP/FI 2.0 was mostly halted. \r\n\r\n', 6, 1),
(5, 'Mickey', '250.00', 'Products/1222928491_t-shirt5.jpg', 'One of the biggest strengths of PHP 3.0 was its strong extensibility features. In addition to providing end users with a solid infrastructure for lots of different databases, protocols and APIs, PHP 3.0\'s extensibility features attracted dozens of developers to join in and submit new extension modules. Arguably, this was the key to PHP 3.0\'s tremendous success. Other key features introduced in PHP 3.0 were the object oriented syntax support and the much more powerful and consistent language syntax. \r\n\r\n', 6, 1),
(6, 'Brown', '1000.00', 'Products/1222928548_watch1.jpg', 'The whole new language was released under a new name, that removed the implication of limited personal use that the PHP/FI 2.0 name held. It was named plain \'PHP\', with the meaning being a recursive acronym - PHP: Hypertext Preprocessor. \r\n\r\nBy the end of 1998, PHP grew to an install base of tens of thousands of users (estimated) and hundreds of thousands of Web sites reporting it installed. At its peak, PHP 3.0 was installed on approximately 10% of the Web servers on the Internet. \r\n\r\nPHP 3.0 was officially released in June 1998, after having spent about 9 months in public testing. \r\n\r\n', 7, 1),
(7, 'Glod plate', '700.00', 'Products/1222928595_watch2.jpg', 'By the winter of 1998, shortly after PHP 3.0 was officially released, Andi Gutmans and Zeev Suraski had begun working on a rewrite of PHP\'s core. The design goals were to improve performance of complex applications, and improve the modularity of PHP\'s code base. Such applications were made possible by PHP 3.0\'s new features and support for a wide variety of third party databases and APIs, but PHP 3.0 was not designed to handle such complex applications efficiently. \r\n\r\n', 7, 1),
(8, 'SquareSilver', '800.00', 'Products/1222928629_watch3.jpg', 'The new engine, dubbed \'Zend Engine\' (comprised of their first names, Zeev and Andi), met these design goals successfully, and was first introduced in mid 1999. PHP 4.0, based on this engine, and coupled with a wide range of additional new features, was officially released in May 2000, almost two years after its predecessor, PHP 3.0. In addition to the highly improved performance of this version, PHP 4.0 included other key features such as support for many more Web servers, HTTP sessions, output buffering, more secure ways of handling user input and several new language constructs. \r\n\r\n', 7, 1),
(9, 'SlimGlod', '2000.00', 'Products/1222928684_watch4.jpg', 'Today, PHP is being used by hundreds of thousands of developers (estimated), and several million sites report as having it installed, which accounts for over 20% of the domains on the Internet. \r\n\r\nPHP\'s development team includes dozens of developers, as well as dozens others working on PHP-related projects such as PEAR and the documentation project. \r\n\r\nPHP 5\r\nPHP 5 was released in July 2004 after long development and several pre-releases. It is mainly driven by its core, the Zend Engine 2.0 with a new object model and dozens of other new features.\r\n', 7, 1),
(10, 'white...parallel', '500.00', 'Products/1229482952_70147596qd6o5wl3img5393lp2.jpg', 'Feminists fume at Neninthe\r\nFeminists and female politicians raised their objection on the poster publicity of Puri Jagan’s latest film Neninthe. Women activist Sandhya, politicians Nannapaneni Raja Kumari and Ganga Bhavani strongly objected to scantly clad heroine being projected in poster designs. Their protest comes in the wake of eve-teasing and atrocities against women. The more they protest, the more it is going to help the publicity of 18 December release Neninthe.\r\n\r\nWill ticket hike GO be issued?\r\nYSR announced that he would be issuing a GO (Government Order) which will allow the exhibitors to hike the ticket price up to Rs. 50/- for upper stalls. However, the GO is not released so far. Neninthe – a big film- is all set to release on 18 December. And Neninthe is unlikely to receive this benefit. We have to wait and see if this GO will be released before Nagarjuna King’s release (24 December). A movie that is capable of collecting 10 crores in the first week will collected only 7 crores if the prices are not hiked.\r\n\r\nAshta Chemma Bhargavi murdered\r\n\r\n', 8, 0),
(11, 'Elite 18', '25.00', 'Products/1232595847_photo1.gif', 'PHP/FI\r\nPHP succeeds an older product, named PHP/FI. PHP/FI was created by Rasmus Lerdorf in 1995, initially as a simple set of Perl scripts for tracking accesses to his online resume. He named this set of scripts \'Personal Home Page Tools\'. As more functionality was required, Rasmus wrote a much larger C implementation, which was able to communicate with databases, and enabled users to develop simple dynamic Web applications. Rasmus chose to » release the source code for PHP/FI for everybody to see, so that anybody can use it, as well as fix bugs in it and improve the code. \r\n\r\nPHP/FI, which stood for Personal Home Page / Forms Interpreter, included some of the basic functionality of PHP as we know it today. It had Perl-like variables, automatic interpretation of form variables and HTML embedded syntax. The syntax itself was similar to that of Perl, albeit much more limited, simple, and somewhat inconsistent. \r\n\r\nBy 1997, PHP/FI 2.0, the second write-up of the C implementation, had a cult of several thousand users around the world (estimated), with approximately 50,000 domains reporting as having it installed, accounting for about 1% of the domains on the Internet. While there were several people contributing bits of code to this project, it was still at large a one-man project. \r\n\r\nPHP/FI 2.0 was officially released only in November 1997, after spending most of its life in beta releases. It was shortly afterwards succeeded by the first alphas of PHP 3.0. \r\n\r\nPHP 3\r\n', 9, 0),
(12, 'fdgd', '32.00', 'Products/1237802699_lamp_Xampp_php_mysql.jpg', 'sdfadsfsd sdf s', 1, 0),
(13, 'hritik', '64644.00', '', 'wnrqwpnr', 11, 0);

-- --------------------------------------------------------

--
-- Table structure for table `student`
--

CREATE TABLE `student` (
  `id` int(11) NOT NULL,
  `roll` varchar(15) NOT NULL,
  `name` varchar(30) NOT NULL,
  `status` varchar(10) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `student`
--

INSERT INTO `student` (`id`, `roll`, `name`, `status`) VALUES
(1, '16cs13', 'partha', ''),
(2, '16cs13', 'partha', ''),
(4, '16cs13', 'hritik', '');

--
-- Indexes for dumped tables
--

--
-- Indexes for table `bs_admin`
--
ALTER TABLE `bs_admin`
  ADD PRIMARY KEY (`Id`);

--
-- Indexes for table `bs_categories`
--
ALTER TABLE `bs_categories`
  ADD PRIMARY KEY (`Id`);

--
-- Indexes for table `bs_customers`
--
ALTER TABLE `bs_customers`
  ADD PRIMARY KEY (`Id`);

--
-- Indexes for table `bs_orders`
--
ALTER TABLE `bs_orders`
  ADD PRIMARY KEY (`Id`);

--
-- Indexes for table `bs_products`
--
ALTER TABLE `bs_products`
  ADD PRIMARY KEY (`Id`);

--
-- Indexes for table `student`
--
ALTER TABLE `student`
  ADD PRIMARY KEY (`id`);

--
-- AUTO_INCREMENT for dumped tables
--

--
-- AUTO_INCREMENT for table `bs_admin`
--
ALTER TABLE `bs_admin`
  MODIFY `Id` int(5) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=3;

--
-- AUTO_INCREMENT for table `bs_categories`
--
ALTER TABLE `bs_categories`
  MODIFY `Id` int(5) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=12;

--
-- AUTO_INCREMENT for table `bs_customers`
--
ALTER TABLE `bs_customers`
  MODIFY `Id` int(5) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=12;

--
-- AUTO_INCREMENT for table `bs_orders`
--
ALTER TABLE `bs_orders`
  MODIFY `Id` int(5) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=12;

--
-- AUTO_INCREMENT for table `bs_products`
--
ALTER TABLE `bs_products`
  MODIFY `Id` int(5) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=14;

--
-- AUTO_INCREMENT for table `student`
--
ALTER TABLE `student`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=5;
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
