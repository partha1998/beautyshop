<?php
	require_once("Lib/Config.php");
	if(isset($_SESSION['MyList'])){
		$cart = $_SESSION['MyList'];
		$pid = $_GET['pid'];
		$qty = $cart[$pid];
		$qty--;
		if($qty ==0){
			unset($cart[$pid]);
		}else{
			$cart[$pid] = $qty;
		}
		$_SESSION['MyList'] = $cart;
	}
	header("Location: ShowCart.php");
?>