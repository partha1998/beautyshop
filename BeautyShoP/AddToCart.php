<?php
	require_once("Lib/Config.php");
	$pid = $_GET['pid'];
	if(!isset($_SESSION['MyList'])){
		$cart = array();
		$cart[$pid] = 1;
		$_SESSION['MyList'] = $cart;
	}else{
		$cart = $_SESSION['MyList'];
		if(array_key_exists($pid,$cart)){
			$cart[$pid]++;
		}else{
			$cart[$pid] = 1;
		}
		$_SESSION['MyList'] = $cart;
	}
	header("Location: ShowCart.php");
?>